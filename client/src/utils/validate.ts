import * as validator from 'validator';

// ERRORS
export const ERRORS = {
  DEFAULT: {
    email: 'Email is required',
    name: 'Field is required',
    password: 'Password is required',
  },
  INVALID: {
    email: 'Email is not valid. Example: hello@gmail.com',
  },
};

export interface IValidate {
  name?: string;
  email?: string;
  password?: string;
  submitState?: boolean;
}

const validateOnEmpty = (str: string | null | undefined): boolean => Boolean(str);

const validateEmail = (str: string | null | undefined):boolean => {
  let isValid = true;
  if (str && !validator.isEmail(str)) {
    isValid = false;
  }
  return isValid;
};

export default (values: IValidate) => {
  const errors: IValidate = {};

  const isValidName = validateOnEmpty(values.name);
  const isValidPassword = validateOnEmpty(values.password);
  const isEmptyEmail = validateOnEmpty(values.email);
  const isValidEmail = validateEmail(values.email);

  if (!isValidName) {
    errors.name = ERRORS.DEFAULT.name;
  }

  if (!isValidPassword) {
    errors.password = ERRORS.DEFAULT.password;
  }

  if (!isEmptyEmail) {
    errors.email = ERRORS.DEFAULT.email;
  }

  if (!isValidEmail) {
    errors.email = ERRORS.INVALID.email;
  }

  return errors;
};
