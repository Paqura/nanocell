import * as React from 'react';

import Header from './Header';
import Footer from './Footer';

interface IProps {
  children: React.ReactNode;
  history: {
    push(url: string): void;
  };
}

const layout = (props: IProps) => {
  const { children } = props;
  return (
    <div>
      <Header />
      {children}
      <Footer />
    </div>
  );
};

export default layout;
