import styledComponents, { css } from 'styled-components';

interface IProps {
  primary?: boolean;
  secondary?: boolean;
  action?: any;
}

const button = styledComponents.button<IProps>`
  cursor: pointer;
  background-color: transparent;
  border: 1px solid transparent;
  border-radius: 4px;
  font-size: 18px;
  letter-spacing: 1px;
  padding: 8px 16px;
  transition: 240ms ease;

  ${props => props.primary && css`
    background: ${props.theme.primary};
    color: ${props.theme.secondary};

    &:hover {
      background: ${props.theme.secondary};
      color: ${props.theme.primary};
      border-color: ${props.theme.primary}
    }
  `}

  ${props => props.secondary && css`
    background: ${props.theme.secondary};
    color: ${props.theme.primary};
    border-color: ${props.theme.primary}

    &:hover {
      background: ${props.theme.lighten};
    }
  `}
`;

export default button;
