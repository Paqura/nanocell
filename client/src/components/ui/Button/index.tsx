import * as React from 'react';
import Button from './Button';

class UIButtonList extends React.Component {
  public render() {
    return (
      <div>
        <p>
          <Button primary={true}>
            Primary
          </Button>
        </p>
        <p>
          <Button secondary={true}>
            Secondary
          </Button>
        </p>
      </div>
    );
  }
}

export { UIButtonList };
export default Button;
