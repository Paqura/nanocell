export interface ITheme {
  primary: string;
  secondary: string;
  lighten: string;
}

export const mainTheme: ITheme = {
  lighten: '#f0f1f8',
  primary: '#3f46ad',
  secondary: '#ffffff',
};
