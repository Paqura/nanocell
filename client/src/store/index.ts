import reduxSaga  from 'redux-saga';
import reduxLogger from 'redux-logger';
import { applyMiddleware, createStore } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import createBrowserHistory from 'history/createBrowserHistory';

// REDUCER
import reducer from './reducer';
// SAGA
import { saga as rootSaga } from './saga';

// HISTORY
export const appHistory = createBrowserHistory();

// MIDDLEWARES
const customRouterMiddleware = routerMiddleware(appHistory);
const sagaMiddleware = reduxSaga();

// ENHANCER
const enhancer = () => {
  if (process.env.NODE_ENV === 'production') {
    return applyMiddleware(sagaMiddleware, customRouterMiddleware);
  }
  return applyMiddleware(sagaMiddleware, customRouterMiddleware, reduxLogger);
};

// STORE
const store = createStore(reducer, enhancer());
sagaMiddleware.run(rootSaga);

export default store;
