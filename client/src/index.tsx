import * as React from 'react';
import * as ReactDOM from 'react-dom';
import styledNormalize from 'styled-normalize';
import { injectGlobal } from 'styled-components';
import webfontloader from 'webfontloader';

import registerServiceWorker from './registerServiceWorker';
import App from './App';

webfontloader.load({
  google: {
    families: ['Roboto:300,500,700', 'sans-serif'],
  },
});

injectGlobal`
  ${styledNormalize}

  body {
    padding: 0;
  }
`;

ReactDOM.render(
  <App />,
  document.getElementById('root') as HTMLElement,
);
registerServiceWorker();
