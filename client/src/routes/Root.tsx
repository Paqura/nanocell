import * as React from 'react';
import { Switch, Route, withRouter } from 'react-router';
import Layout from '../components/Layout';

// FAKE ROUTES
const app = () => <h1>App</h1>;
const about = () => <h1>About</h1>;

class Root extends React.Component<any, any> {
  public render() {
    const { history } = this.props;
    return (
      <Layout history={history}>
        <Switch>
          <Route exact={true} path="/" component={app}/>
          <Route exact={true} path="/about" component={about}/>
        </Switch>
      </Layout>
    );
  }
}

export default withRouter(Root);
