import * as React from 'react';
import { Provider } from 'react-redux';
import { ThemeProvider } from 'styled-components';
import { ConnectedRouter } from 'react-router-redux';
import { HashRouter as Router } from 'react-router-dom';

// STORE

import store, { appHistory } from 'src/store';

// WHILE DEV
declare global {
  interface Window {
    store: any;
  }
}

// THEME
import { mainTheme } from './theme';

// ROOT ROUTE
import Root from 'src/routes/Root';

class App extends React.Component {
  public render() {
    return (
      <ThemeProvider theme={mainTheme}>
        <Provider store={store}>
          <ConnectedRouter history={appHistory}>
            <Router>
              <Root />
            </Router>
          </ConnectedRouter>
        </Provider>
      </ThemeProvider>
    );
  }
}

window.store = store;

export default App;
