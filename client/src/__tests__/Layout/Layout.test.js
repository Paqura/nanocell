import React from 'react';
import { create } from 'react-test-renderer';
import { shallow } from 'enzyme';

import Header from '../../components/Layout/Header';
import Footer from '../../components/Layout/Footer';

describe('Layout tests', () => {
  describe('Header tests', () => {
    const headerComponent = create(
      <Header />
    );

    it('should be return correct snapshot for Header', () => {      
      const tree = headerComponent.toJSON();
      expect(tree).toMatchSnapshot();
    });
  }); 

  describe('Footer tests', () => {
    const footerComponent = create(
      <Footer />
    );

    it('should be return correct snapshot for Footer', () => {
      const tree = footerComponent.toJSON();
      expect(tree).toMatchSnapshot();
    });
  });
});