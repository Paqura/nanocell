import isEmpty from '../../utils/isEmpty';

describe('Test of the `isEmpty` function', () => {
  it('should return `true`', () => {
    const answer1 = isEmpty('');
    const answer2 = isEmpty([]);
    const answer3 = isEmpty({});
    const answer4 = isEmpty(null);
    const answer5 = isEmpty(undefined);
    expect(answer1).toEqual(true);
    expect(answer2).toEqual(true);
    expect(answer3).toEqual(true);
    expect(answer4).toEqual(true);
    expect(answer5).toEqual(true);
  });

  it('should return `false`', () => {
    const answer1 = isEmpty('hello test');
    const answer2 = isEmpty(['hello', 'test']);
    const answer3 = isEmpty(666);
    expect(answer1).toEqual(false);
    expect(answer2).toEqual(false);
    expect(answer3).toEqual(false);
  });
});