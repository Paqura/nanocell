import validate, { ERRORS } from '../../utils/validate';

describe('Test of validate function', () => {
  it('should return `default errors on every field`', () => {
    const answer = validate({});
    expect(answer).toEqual({...ERRORS.DEFAULT});
  });

  it('should return `error on email`', () => {
    const values = {
      name: 'Test',
      email: 'notEmail',
      password: 'testpassword'
    };

    const values2 = {
      name: 'Test',
      email: '123@mail',
      password: 'testpassword'
    };

    const emailError = {
      email: ERRORS.INVALID.email
    };

    const answer1 = validate(values);
    const answer2 = validate(values2);
    expect(answer1).toEqual(emailError);
    expect(answer2).toEqual(emailError);
  });

  it('should return `password is required`', () => {
    const answer = validate({
      name: 'Test',
      email: 'test@mail.com',
      password: ''
    });

    expect(answer).toEqual({
      password: ERRORS.DEFAULT.password
    })
  });
});